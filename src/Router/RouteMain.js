import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import router from "./Path";
const RouterMain = () => {
  return (
    <>
      <Router>
        <Switch>
          {router.map((route) => {
            return (
              <Route
                key={route.path}
                path={route.path}
                component={route.component}
                exact={typeof route.exact === "undefined" ? true : route.exact}
              />
            );
          })}
        </Switch>
      </Router>
    </>
  );
};

export default RouterMain;
