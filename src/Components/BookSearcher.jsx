// [] First Challenge ==> Loading Line 21 and 22
// [] Second and Third Challenge ==> Cancel Previous Api (Abort)

import axios from "axios";
import React, { useState, useEffect } from "react";
import No_Img from "../assets/Images/No_image.jpg";
const BookSearcher = () => {
  // State
  const [data, setData] = useState();
  const [keyword, setKeyword] = useState("");
  const [loadingData, setLoadingData] = useState(false);
  const [cancel, setCancel] = useState();
  //Functions
  let cancelTokenSource;

  const onChangeInput = (e) => {
    setKeyword(e.target.value);
  };

  // hook

  useEffect(async () => {
    let cancelTokenSource;

    if (keyword.length >= 3) {
      setLoadingData(true);
      setData();
      if (cancel !== undefined) {
        cancel.cancel();
      }
      cancelTokenSource = axios.CancelToken.source();
      setCancel(cancelTokenSource);

      axios
        .get(
          `https://www.googleapis.com/books/v1/volumes?
          country=US&projection=lite&printType=books&key=
          AIzaSyD6SlU9JUr7Z-3SOOy0TfZTJtqv_EEqfZY&q=intitle:${keyword}&startIndex=0&maxResults=5`,
          { cancelToken: cancelTokenSource.token }
        )
        .then((res) => {
          setData(res.data.items);
          setLoadingData(false);
        })
        .catch((err) => console.log(err.response));
    }
  }, [keyword]);

  return (
    <>
      <div className="BookSearcher">
        <div className="container">
          <div className="row">
            <div className="d-flex flex-column align-items-center mt-5">
              <div className="form-group input_box">
                <input
                  onChange={(e) => onChangeInput(e)}
                  className="form-control rounded-pill"
                  type="text"
                  placeholder="Search Book"
                />
              </div>
              {keyword.length > 0 && (
                <div className="border w-50 rounded data_box">
                  {keyword.length > 2 ? (
                    data ? (
                      data.map((item, index) => {
                        let lowerCase = item.volumeInfo.title.toLowerCase();
                        let SameWordArr = lowerCase.split(" ");

                        return (
                          <>
                            <div
                              key={index}
                              className="d-flex align-items center"
                            >
                              {item.volumeInfo.readingModes.image ? (
                                <img
                                  className="book_picture"
                                  src={
                                    item.volumeInfo.imageLinks?.smallThumbnail
                                  }
                                  alt={item.volumeInfo.title}
                                />
                              ) : (
                                <img
                                  className="book_picture"
                                  src={No_Img}
                                  alt={item.volumeInfo.title}
                                />
                              )}

                              <div className="mt-1 pl-2">
                                {SameWordArr &&
                                  SameWordArr.map((word) => {
                                    return (
                                      <>
                                        {word.includes(
                                          keyword.toLowerCase()
                                        ) ? (
                                          <b>
                                            {word.charAt(0).toUpperCase() +
                                              word.slice(1) +
                                              " "}
                                          </b>
                                        ) : (
                                          <span>
                                            {word.charAt(0).toUpperCase() +
                                              word.slice(1) +
                                              " "}
                                          </span>
                                        )}
                                      </>
                                    );
                                  })}
                                <p className="mt-2 pl-2">
                                  {item.volumeInfo.authors?.map(
                                    (author, indexA) => {
                                      return (
                                        author +
                                        (indexA <
                                          item.volumeInfo.authors.length - 1 &&
                                          ", ")
                                      );
                                    }
                                  )}
                                </p>
                              </div>
                            </div>
                          </>
                        );
                      })
                    ) : (
                      <div className="d-flex justify-content-center pt-3 py-2">
                        {loadingData ? (
                          <div className="spinner-border"></div>
                        ) : (
                          <p>Not Found</p>
                        )}
                      </div>
                    )
                  ) : (
                    <div className="d-flex justify-content-center pt-3 py-2">
                      <p>For Search Need 3 Letters</p>
                    </div>
                  )}
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default BookSearcher;
